# Morrish
Commands that makes things easier for me, mostly wrappers around long tedious commands.

### Script Usage

1. Clone it
2. Source it using your favourite profile:
    - `echo -e "\nsource ~/dev/morrish/index.sh" >> ~/.bash_profile && source ~/.bash_profile`

### Hook Usage

1. Clone it
2. Update your Git config to point at the hooks:
    - `git config --global core.hooksPath ~/dev/morrish/hooks`

Updating it only requires a Git pull of the project